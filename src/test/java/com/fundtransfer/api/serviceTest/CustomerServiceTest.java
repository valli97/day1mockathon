package com.fundtransfer.api.serviceTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.fundtransfer.api.model.Customer;
import com.fundtransfer.api.repository.CustomerRepository;
import com.fundtransfer.api.service.CustomerService;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CustomerServiceTest {
	@InjectMocks
	CustomerService customerService;
	@Mock
	CustomerRepository customerRepository;

	@Test
	public void saveCustomerTest() {
		Customer customer = new Customer();
		customer.setCustomerFirstName("sai");
		customer.setCustomerLastName("jaya");
		customer.setCustomerUserName("saijaya");
		customer.setCustomerCity("nellore");
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		Customer customer1 = customerService.saveCustomer(customer);
		Assert.assertNotNull(customer1);
		Assert.assertEquals(customer1,customer);
	}
	
	@Test
	public void getAllCustomerListTest() {
		List<Customer> c=new ArrayList();
		Customer customer = new Customer();
		customer.setCustomerFirstName("sai");
		customer.setCustomerLastName("jaya");
		customer.setCustomerUserName("saijaya");
		customer.setCustomerCity("nellore");
		c.add(customer);
        Customer customer1 = new Customer();
		customer.setCustomerFirstName("Mounika");
		customer.setCustomerLastName("Ganesh");
		customer.setCustomerUserName("Mouniganu");
		customer.setCustomerCity("pune");
		c.add(customer);
		Mockito.when(customerRepository.findAll()).thenReturn(c);
		List<Customer> stu = customerService.getAllCustomerList();
		Assert.assertNotNull(stu);
		Assert.assertEquals(2, stu.size());
	}
	
@Test
public void findByCustomerIdTest() {
	Customer customer=new Customer();
	customer.setCustomerFirstName("sai");
	customer.setCustomerLastName("jaya");
	customer.setCustomerUserName("saijaya");
	customer.setCustomerCity("nellore");
	customer.setCustomerId((long) 123456789);
	Customer customer1=new Customer();
	customer1.setCustomerFirstName("Mounika");
	customer1.setCustomerLastName("Ganesh");
	customer1.setCustomerUserName("Mouniganu");
	customer1.setCustomerCity("pune");
	customer1.setCustomerId((long) 987654321);

	Mockito.when(customerRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(customer));
	Optional<Customer> res = customerService.findByCustomerId(1l);
	Assert.assertNotNull(res);
	Assert.assertEquals(customer.getCustomerFirstName(), res.get().getCustomerFirstName());
}
	
@Test
public void findByCustomerIdTestNeg() {
	Customer customer=new Customer();
	customer.setCustomerFirstName("sai");
	customer.setCustomerLastName("jaya");
	customer.setCustomerUserName("saijaya");
	customer.setCustomerCity("nellore");
	customer.setCustomerId((long) 123456789);
	Customer customer1=new Customer();
	customer1.setCustomerFirstName("Mounika");
	customer1.setCustomerLastName("Ganesh");
	customer1.setCustomerUserName("Mouniganu");
	customer1.setCustomerCity("pune");
	customer1.setCustomerId((long) 987654321);

	Mockito.when(customerRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(customer));
	Optional<Customer> res = customerService.findByCustomerId(-1l);
	Assert.assertNotNull(res);
	Assert.assertEquals(customer.getCustomerFirstName(), res.get().getCustomerFirstName());
}
@Test
public void updateCustomerTest() {
	Customer customer = new Customer();
	customer.setCustomerFirstName("sai");
	customer.setCustomerLastName("jaya");
	customer.setCustomerUserName("saijaya");
	customer.setCustomerCity("nellore");
	customer.setCustomerId((long) 987654321);

	Mockito.when(customerRepository.save(customer)).thenReturn(customer);
	Customer customer1 = customerService.saveCustomer(customer);
	Assert.assertNotNull(customer1);
	Assert.assertEquals(customer1,customer);
}
}