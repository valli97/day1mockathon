package com.fundtransfer.api.controllerTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fundtransfer.api.controller.AccountController;
import com.fundtransfer.api.model.Account;
import com.fundtransfer.api.model.BeneficiaryAcoounts;
import com.fundtransfer.api.service.AccountService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AccountControllerTest {
	@InjectMocks
	AccountController accountController;
	@Mock
	AccountService accountService;
	MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(accountController).build();
	}

	@Test
	public void testgetAllbeneficiaryAcoountsMvc() throws Exception {
		List<Account> a = new ArrayList();
		Account account = new Account();
		account.setAccountHolderName("jaya");
		account.setBalance(1500);
		a.add(account);
		Account account1 = new Account();
		account1.setAccountHolderName("sai");
		account1.setBalance(2500);
		a.add(account1);
		Mockito.when(accountService.getAllbeneficiaryAcoountsList()).thenReturn(a);

		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/accounts4")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);
	}

	@Test
	public void testGetAccountMvc() throws Exception {
		Account account = new Account();
		account.setAccountHolderName("jaya");
		account.setBalance(1500);
		BeneficiaryAcoounts b = new BeneficiaryAcoounts();
		b.setBalance(1500);
		b.setBeneficiaryAcoountNumber("1234567");
		b.setAccount(account);

		Mockito.when(accountService.getAccount((Mockito.anyString()))).thenReturn(account);

		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/accounts5")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);
	}

	@Test
	public void testsaveAccountMvc() throws Exception {
		Account account = new Account();
		account.setAccountHolderName("jaya");
		account.setBalance(1500);
		// account.setBeneficiaryAcoounts(beneficiaryAcoounts);
		BeneficiaryAcoounts b = new BeneficiaryAcoounts();
		b.setBalance(1500);
		b.setBeneficiaryAcoountNumber("1234567");
		b.setAccount(account);

		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.post("/accounts1")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);
	}

	@Test
	public void testcreateAccountMvc() throws Exception{
		Account account = new Account();
		account.setAccountHolderName("jaya");
		account.setBalance(1500);
		BeneficiaryAcoounts b = new BeneficiaryAcoounts();
		b.setBalance(1500);
		b.setBeneficiaryAcoountNumber("1234567");
		b.setAccount(account);

		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.post("/accounts")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);
	}
	
}

