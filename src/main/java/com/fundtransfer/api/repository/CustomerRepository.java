package com.fundtransfer.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fundtransfer.api.model.Customer;
import com.fundtransfer.api.model.Login;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	//public Optional<Customer> findByCustomerUserNameAndCustomerPassword(String customerUsername,String customerPassword);
	public Customer findByCustomerUserNameAndCustomerPassword(String customerUsername,String customerPassword);
	

}
