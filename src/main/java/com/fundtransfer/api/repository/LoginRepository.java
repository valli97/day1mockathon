package com.fundtransfer.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fundtransfer.api.model.Login;



@Repository
public interface LoginRepository extends JpaRepository<Login, Integer> {

	//public Login findByLoginUsernameAndLoginPassword(String loginUsername, String loginPassword);
	
	public Optional<Login> findByLoginUsernameAndLoginPassword(String loginUsername,String loginPassword);
	
	//public Optional<Login> findByLoginUsername(loginUsername);

}
