package com.fundtransfer.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fundtransfer.api.model.BeneficiaryAcoounts;

public interface BeneficiaryRepository extends JpaRepository<BeneficiaryAcoounts, String> {

}
