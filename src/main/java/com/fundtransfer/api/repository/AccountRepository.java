package com.fundtransfer.api.repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fundtransfer.api.exception.DuplicateAccountIdException;
import com.fundtransfer.api.model.Account;

public interface AccountRepository extends JpaRepository<Account, String>{
	
	
}
