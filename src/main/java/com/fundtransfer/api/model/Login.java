package com.fundtransfer.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//@Table(name = "Login_tbL")
public class Login {

	@Id
	@Column
	private Integer loginId;
	@Column
	private String loginUsername;
	@Column
	private String loginPassword;
	@Column
	private boolean isLogin;

	public Login() {
	}

	public Login(Integer loginId, String loginUsername, String loginPassword,boolean isLogin) {
		super();
		this.loginId = loginId;
		this.loginUsername = loginUsername;
		this.loginPassword = loginPassword;
		this.isLogin=isLogin;
	}
	
	public Login(Integer loginId, String loginUsername, String loginPassword) {
		super();
		this.loginId = loginId;
		this.loginUsername = loginUsername;
		this.loginPassword = loginPassword;
		
	}

	public Integer getLoginId() {
		return loginId;
	}

	public void setLoginId(Integer loginId) {
		this.loginId = loginId;
	}

	public String getLoginUsername() {
		return loginUsername;
	}

	public void setLoginUsername(String loginUsername) {
		this.loginUsername = loginUsername;
	}

	public String getLoginPassword() {
		return loginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	public boolean isLogin() {
		return isLogin;
	}

	public void setLogin(boolean isLogin) {
		this.isLogin = isLogin;
	}

	@Override
	public String toString() {
		return "Login [loginId=" + loginId + ", loginUsername=" + loginUsername + ", loginPassword=" + loginPassword
				+ "]";
	}

}
