package com.fundtransfer.api.model;

import java.util.List;
import java.util.Set;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long customerId;
	@Column
	private String customerFirstName;
	@Column
	private String customerLastName;
	@Column
	private String customerAdharNumber;
	@Column
	private String customerUserName;
	@Column
	private String customerMobNumber;
	@Column
	private String customerCity;
	@Column
	private String customerCountryCode;
	@Column
	private String customerPassword;
	

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Account> acoount;

	//@JsonManagedReference
	public Set<Account> getAcoounts() {
		return acoount;
	}

	public void setAcoounts(Set<Account> acoount) {
		this.acoount = acoount;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	public String getCustomerAdharNumber() {
		return customerAdharNumber;
	}

	public void setCustomerAdharNumber(String customerAdharNumber) {
		this.customerAdharNumber = customerAdharNumber;
	}

	public String getCustomerUserName() {
		return customerUserName;
	}

	public void setCustomerUserName(String customerUserName) {
		this.customerUserName = customerUserName;
	}

	public String getCustomerMobNumber() {
		return customerMobNumber;
	}

	public void setCustomerMobNumber(String customerMobNumber) {
		this.customerMobNumber = customerMobNumber;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomerCountryCode() {
		return customerCountryCode;
	}

	public void setCustomerCountryCode(String customerCountryCode) {
		customerCountryCode = customerCountryCode;
	}

	public String getCustomerPassword() {
		return customerPassword;
	}

	public void setCustomerPassword(String customerPassword) {
		this.customerPassword = customerPassword;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", customerFirstName=" + customerFirstName + ", customerLastName="
				+ customerLastName + ", customerAdharNumber=" + customerAdharNumber + ", customerUserName="
				+ customerUserName + ", customerMobNumber=" + customerMobNumber + ", customerCity=" + customerCity
				+ ", customerCountryCode=" + customerCountryCode + ", customerPassword=" + customerPassword + "]";
	}

}
