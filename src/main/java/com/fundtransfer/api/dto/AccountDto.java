package com.fundtransfer.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountDto {
	
	@JsonProperty(value = "accountNumber")
	private String accountNumber;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

}
