package com.fundtransfer.api.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransactionHistoryDto {

	@JsonProperty(value = "fromSalryAccountNumber")
	private String fromSalryAccountNumber;
	@JsonProperty(value = "toBenificiaryAccountNumber")
	private String toBenificiaryAccountNumber;
	@JsonProperty(value = "amount")
	private double amount;
	
	
	public String getFromSalryAccountNumber() {
		return fromSalryAccountNumber;
	}
	public void setFromSalryAccountNumber(String fromSalryAccountNumber) {
		this.fromSalryAccountNumber = fromSalryAccountNumber;
	}
	public String getToBenificiaryAccountNumber() {
		return toBenificiaryAccountNumber;
	}
	public void setToBenificiaryAccountNumber(String toBenificiaryAccountNumber) {
		this.toBenificiaryAccountNumber = toBenificiaryAccountNumber;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

}
