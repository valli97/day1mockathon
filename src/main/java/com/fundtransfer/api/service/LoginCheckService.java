package com.fundtransfer.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fundtransfer.api.exception.InvalidCredentialException;
import com.fundtransfer.api.model.Customer;
import com.fundtransfer.api.model.Login;
import com.fundtransfer.api.repository.CustomerRepository;
import com.fundtransfer.api.repository.LoginRepository;

@Service
public class LoginCheckService {

	@Autowired
	LoginRepository loginRepository;

	@Autowired
	CustomerRepository customerRepository;

	Customer customer = new Customer();

	public ResponseEntity<Login> customerLogin(Login login) {

		String loginUsername = customer.getCustomerUserName();
		String loginPassword = customer.getCustomerPassword();
		Optional<Login> loginUser = loginRepository.findByLoginUsernameAndLoginPassword(loginUsername, loginPassword);

		Login loginSuccessful = (Login) loginUser.get();
		loginSuccessful.setLogin(true);
		loginRepository.save(loginSuccessful);

		return new ResponseEntity<Login>(loginSuccessful, HttpStatus.OK);

	}

	public ResponseEntity<Login> customerLogout(Login login) {

		String loginUsername = customer.getCustomerUserName();
		String loginPassword = customer.getCustomerPassword();
		Optional<Login> loginUser = loginRepository.findByLoginUsernameAndLoginPassword(loginUsername, loginPassword);

		Login logoutSuccessful = (Login) loginUser.get();
		logoutSuccessful.setLogin(false);
		loginRepository.save(logoutSuccessful);
		if (!loginUser.isPresent()) {
			throw new InvalidCredentialException(" check login details , Pls try again ");
		}
		return new ResponseEntity<Login>(logoutSuccessful, HttpStatus.OK);

	}

	/*
	 * public boolean checkLoginValidation(String loginUsername) { String
	 * loginUsername = customer.getCustomerUserName();
	 * 
	 * Optional<Login> loginUser =
	 * loginRepository.findByLoginUsername(loginUsername); if
	 * (!loginUser.isPresent()) { throw new
	 * InvalidCredentialException(" check login details "); } Login loginSuccessful
	 * = (Login) loginUser.get(); if (loginSuccessful.isLogin() == false) { throw
	 * new InvalidCredentialException(" Not Logged "); } if
	 * (loginSuccessful.isLogin() == true) { return true; } return false; }
	 */
	/*
	 * public boolean valiateUserLogin(String loginUsername, String loginPassword) {
	 * 
	 * Login checkLogin =
	 * loginRepository.findByLoginUsernameAndLoginPassword(loginUsername,
	 * loginPassword);
	 * 
	 * if (checkLogin != null && checkLogin.getLoginUsername().equals(loginUsername)
	 * && checkLogin.getLoginPassword().equals(loginPassword)) {
	 * 
	 * return true; } else { throw new InvalidCredentialException(
	 * "Invalid Login Details Pls try again ::" + loginUsername + " :: " +
	 * loginPassword); } }
	 * 
	 * public boolean valiateCustomerLogin(String loginUsername, String
	 * loginPassword) {
	 * 
	 * Login checkLogin =
	 * loginRepository.findByLoginUsernameAndLoginPassword(loginUsername,
	 * loginPassword);
	 * 
	 * if (checkLogin != null && checkLogin.getLoginUsername().equals(loginUsername)
	 * && checkLogin.getLoginPassword().equals(loginPassword)) {
	 * 
	 * return true; } else { throw new InvalidCredentialException(
	 * "Invalid Login Details Pls try again ::" + loginUsername + " :: " +
	 * loginPassword); } }
	 * 
	 * public Login saveLoginDetails(Login login) { Login saveLogin =
	 * loginRepository.save(login); return saveLogin;
	 * 
	 * }
	 */

	/*
	 * public Login valiateUserLoginDetails(String loginUsername, String
	 * loginPassword) {
	 * 
	 * Login checkLogin =
	 * loginRepository.findByLoginUsernameAndLoginPassword(loginUsername,
	 * loginPassword);
	 * 
	 * if (loginUsername.equals(checkLogin.getLoginUsername()) &&
	 * loginPassword.equals(checkLogin.getLoginPassword())) {
	 * 
	 * return checkLogin;
	 * 
	 * } else { throw new InvalidCredentialException(
	 * "Invalid Login Details Pls try again ::" + loginUsername + " :: " +
	 * loginPassword); }
	 * 
	 * }
	 */

}
