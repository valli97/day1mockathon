package com.fundtransfer.api.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fundtransfer.api.exception.DuplicateAccountIdException;
import com.fundtransfer.api.exception.ResourceNotFoundException;
import com.fundtransfer.api.model.Account;

import com.fundtransfer.api.model.Customer;
import com.fundtransfer.api.repository.AccountRepository;

@Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;

	private final Map<String, Account> accounts = new ConcurrentHashMap();

	public Account saveAccountDetails(Account account) throws DuplicateAccountIdException {

		Account previousAccount = accounts.putIfAbsent(account.getAccountNumber(), account);
		if (previousAccount != null) {
			throw new DuplicateAccountIdException("Account id " + account.getAccountNumber() + " already exists!");
		}

		return accountRepository.save(account);

	}

	public List<Account> getAllbeneficiaryAcoountsList() {
		return accountRepository.findAll();
	}

	/*
	 * public void createAccount(Account account) throws DuplicateAccountIdException
	 * { Account previousAccount = accounts.putIfAbsent(account.getAccountNumber(),
	 * account); if (previousAccount != null) { throw new
	 * DuplicateAccountIdException("Account id " + account.getAccountNumber() +
	 * " already exists!"); } }
	 */
	public Account getAccount(String accountNumber) {
		return accounts.get(accountNumber);
	}

	public void clearAccounts() {
		accounts.clear();
	}

	/*
	 * public String transfer(String fromAccountId, String toAccountId, BigDecimal
	 * transferAmount) { if (fromAccountId.isEmpty() || toAccountId.isEmpty()) {
	 * throw new ResourceNotFoundException("Account Id cannot be empty/blank"); }
	 * 
	 * if (fromAccountId.equalsIgnoreCase(toAccountId) || transferAmount.longValue()
	 * < 0) { throw new
	 * ResourceNotFoundException("Trasfer Not possible due to wrong parameters"); }
	 * 
	 * Account from = this.getAccount(fromAccountId); Account to =
	 * this.getAccount(toAccountId); if (from != null && to != null) { if
	 * (((from.getBalance().subtract(transferAmount)).longValue()) >
	 * Long.valueOf(0)) {
	 * from.setBalance(from.getBalance().subtract(transferAmount));
	 * to.setBalance(to.getBalance().add(transferAmount));
	 * 
	 * return "Transfer Successful"; } else { throw new
	 * ResourceNotFoundException("Insufficient Balance"); } } else { throw new
	 * ResourceNotFoundException("Account Not Exist"); }
	 * 
	 * }
	 */

}
