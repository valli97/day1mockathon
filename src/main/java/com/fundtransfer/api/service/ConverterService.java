package com.fundtransfer.api.service;

import org.springframework.stereotype.Component;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.fundtransfer.api.dto.AccountDto;
import com.fundtransfer.api.dto.CustomerDto;
import com.fundtransfer.api.dto.TransactionHistoryDto;
import com.fundtransfer.api.model.Account;
import com.fundtransfer.api.model.Customer;
import com.fundtransfer.api.model.TransactionHistory;

@Component
public class ConverterService {
	
	
	@Autowired
	private ModelMapper modelmapper;

	public TransactionHistoryDto converToDto(TransactionHistory transactionHistoryObject) {
		return modelmapper.map(transactionHistoryObject, TransactionHistoryDto.class);
	}

	public AccountDto converToDto(Account accountObject) {
		return modelmapper.map(accountObject, AccountDto.class);
	}
	
	public CustomerDto converToDto(Customer customerObject) {
		return modelmapper.map(customerObject, CustomerDto.class);
	}
	
}
