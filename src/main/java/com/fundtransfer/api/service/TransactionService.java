package com.fundtransfer.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fundtransfer.api.model.Account;
import com.fundtransfer.api.model.BeneficiaryAcoounts;
import com.fundtransfer.api.repository.AccountRepository;
import com.fundtransfer.api.repository.BeneficiaryRepository;

@Service
public class TransactionService {

	@Autowired
	private BeneficiaryRepository beneficiaryRepository;

	@Autowired
	private AccountRepository accountRepository;
	
	public void transferAmount(String fromSalryAccountNumber, String toBenificiaryAccountNumber, double amount) {
		Account fromAccount = accountRepository.getOne(fromSalryAccountNumber);
		BeneficiaryAcoounts toAccount = beneficiaryRepository.getOne(toBenificiaryAccountNumber);
		fromAccount.setBalance(fromAccount.getBalance() - amount);
		toAccount.setBalance(toAccount.getBalance() + amount);
		accountRepository.saveAndFlush(fromAccount);
		beneficiaryRepository.saveAndFlush(toAccount);

	}
	
}
