package com.fundtransfer.api.utility;

import java.security.SecureRandom;
import java.util.Random;

public class UsernameGeneration {
	private static final Random RANDOM = new SecureRandom();

	private static final String ALPHABETUSERNAME = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	public static String generateUsername(int length) {
		StringBuilder returnValue = new StringBuilder(length);
		for (int i = 0; i < length; i++) {
			returnValue.append(ALPHABETUSERNAME.charAt(RANDOM.nextInt(ALPHABETUSERNAME.length())));
		}
		return new String(returnValue);
	}
	
	public static void main(String[] args) {
		int usernameLength=5;
		String username = generateUsername(usernameLength);

	}

}
