package com.fundtransfer.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fundtransfer.api.model.Login;
import com.fundtransfer.api.service.LoginCheckService;

@RestController
public class LoginCheckRestController {

	@Autowired
	private LoginCheckService loginCheckService;

	@PostMapping("/login")
	public ResponseEntity<Login> Login(@RequestBody Login login) {
		ResponseEntity<Login> checkLogin = loginCheckService.customerLogin(login);
		return checkLogin;
	}

	@PostMapping("/logout")
	public ResponseEntity<Login> Logout(@RequestBody Login login) {
		ResponseEntity<Login> checkLogout = loginCheckService.customerLogout(login);
		return checkLogout;
	}

	/*
	 * @GetMapping("/login1") public boolean isLoginCheck(String loginUsername) {
	 * Boolean isLoginFlag = loginCheckService.checkLoginValidation(loginUsername);
	 * return isLoginFlag; }
	 */

	/*
	 * @PostMapping("/login1") public ResponseEntity<String>
	 * saveLiginDetails(@RequestBody Login login) {
	 * loginCheckService.saveLoginDetails(login); return new
	 * ResponseEntity<String>("LoginDetails added successfully",
	 * HttpStatus.CREATED); }
	 */

	/*
	 * @GetMapping("/login1/{loginUsername}/{loginPassword}") public
	 * ResponseEntity<String> loginCheck(@PathVariable("loginUsername") String
	 * loginUsername,
	 * 
	 * @PathVariable("loginPassword") String loginPassword) throws
	 * InvalidCredentialException { Login loginCheck =
	 * loginCheckService.valiateUserLoginDetails(loginUsername, loginPassword);
	 * return new
	 * ResponseEntity<>("Login Successfully now you consume product webservices",
	 * HttpStatus.OK); }
	 */

}