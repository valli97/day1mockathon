package com.fundtransfer.api.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fundtransfer.api.dto.CustomerDto;
import com.fundtransfer.api.exception.InvalidCredentialException;
import com.fundtransfer.api.exception.ResourceNotFoundException;
import com.fundtransfer.api.model.Customer;
import com.fundtransfer.api.model.TransactionHistory;
import com.fundtransfer.api.service.ConverterService;
import com.fundtransfer.api.service.CustomerService;
import com.fundtransfer.api.service.LoginCheckService;
import com.fundtransfer.api.utility.PasswordGeneration;
import com.fundtransfer.api.utility.UsernameGeneration;

@RestController
public class CustomerController {

	@Autowired
	private CustomerService customerService;
	@Autowired
	private ConverterService converterService;
	@Autowired
	private LoginCheckService loginCheckService;

	PasswordGeneration password = password = new PasswordGeneration();

	@PostMapping("/customer")
	@ResponseStatus(HttpStatus.CREATED)
	public CustomerDto saveCustomer(@RequestBody Customer customer) {
		Customer saveCustomer = customerService.saveCustomer(customer);
		return converterService.converToDto(saveCustomer);
	}

	@DeleteMapping("/customer2/{customerId}")
	public ResponseEntity<String> deleteCustomer(@PathVariable Long customerId) {
		customerService.deleteCustomer(customerId);
		return new ResponseEntity<String>("Customer deleted successfully", HttpStatus.OK);
	}

	@PostMapping("/login1/{customerUsername}/{customerPassword}")
	public ResponseEntity<Object> loginCheck(@RequestParam("customerUsername") String customerUsername,
			@RequestParam("customerPassword") String customerPassword) throws InvalidCredentialException {
		Customer loginCheck = customerService.valiateCustomerLoginDetails(customerUsername, customerPassword);

		return new ResponseEntity<>("Login Successfully--Username-- " + loginCheck.getCustomerUserName()
				+ " And --Password--" + loginCheck.getCustomerPassword(), HttpStatus.OK);
	}

	@GetMapping("/customer3/{customerId}")
	public ResponseEntity<Object> getByCustomerId(@PathVariable Long customerId) {
		Optional<Customer> customer = null;
		try {
			customer = customerService.findByCustomerId(customerId);
		} catch (ResourceNotFoundException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Object>(customer, HttpStatus.OK);
	}

	/*
	 * @GetMapping("/customers") public ResponseEntity<List<Customer>>
	 * getAllCustomer(@RequestParam("loginUsername") String loginUsername) {
	 * List<Customer> customerList = null; if
	 * (loginCheckService.checkLoginValidation(loginUsername)) {
	 * customerService.getAllCustomerList(); } return new
	 * ResponseEntity<List<Customer>>(HttpStatus.OK); }
	 */

	/*
	 * @PostMapping(value="/user")
	 * 
	 * @ResponseStatus(HttpStatus.CREATED)
	 * 
	 * @ResponseBody public ResponseEntity<CustomerDto>save(@RequestBody Customer
	 * customer) throws Exception{ String username = password.generateUsername(5);
	 * String pass = password.generatePassword(10);
	 * customer.setCustomerUserName(username); customer.setCustomerPassword(pass);
	 * customerService.saveCustomer(customer); return new <CustomerDto("UserName--"
	 * + customer.getCustomerUserName() + " And" + "  Password--" +
	 * customer.getCustomerPassword(), HttpStatus.OK); }
	 */

}
