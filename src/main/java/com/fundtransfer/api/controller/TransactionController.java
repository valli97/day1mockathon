package com.fundtransfer.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fundtransfer.api.model.TransactionHistory;
import com.fundtransfer.api.service.TransactionService;

@RestController
public class TransactionController {

	@Autowired
	private TransactionService transactionService;

	@PostMapping("/transfer")
	public ResponseEntity<String> amountTransfer(@RequestBody TransactionHistory transactionHistory) {
		transactionService.transferAmount(transactionHistory.getFromSalryAccountNumber(),
				transactionHistory.getToBenificiaryAccountNumber(), transactionHistory.getAmount());
		return new ResponseEntity<String>("fund transfer successfully", HttpStatus.OK);

	}

}
